<div id="cont-menu">
<button id="link-menu" aria-label="ouvrir le menu" aria-expanded="false">
    <span class="burger"></span>
    <span class="cross"></span>
</button>
</div>
<?php
wp_nav_menu( array(
    'theme_location'=> 'menu',
    'menu_id'       => 'access-menu',
    'menu_class'    => 'screen-reader-text',
) );
?>

<div id="cont-menu-full-screen">
    <?php
		if ( has_custom_logo() ) :
			the_custom_logo();
		endif;
	?>
    <nav id="site-navigation" class="main-navigation" role="navigation"  >
        <h2 class="widgettitle">Menu</h2>
        <?php
        wp_nav_menu( array(
            'theme_location' => 'menu-1',
            'menu_id'        => 'main-menu',
        ) );
        ?>
    </nav>
    <ul id="right-menu-full-screen" role="complementary">
        <?php if ( is_active_sidebar( 'menu-full-screen' ) ) : ?>
            <?php dynamic_sidebar( 'menu-full-screen' ); ?>
        <?php endif; ?>
    </ul>
    <ul id="bottom-menu-full-screen" role="complementary">
        <?php if ( is_active_sidebar( 'menu-full-screen-bottom' ) ) : ?>
            <?php dynamic_sidebar( 'menu-full-screen-bottom' ); ?>
        <?php endif; ?>
            
    </ul>

</div>