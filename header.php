<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Susty
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'susty' ); ?></a>
	<a class="skip-link screen-reader-text" href="#menu"><?php esc_html_e( 'Menu', 'susty' ); ?></a>
	<header id="masthead">
		<?php 
			if(is_singular()):
				susty_wp_post_thumbnail(); 
			elseif(is_archive()):?>
			<div class="post-thumbnail">
				<?php
					$term = get_queried_object();
					$image = get_field('image',$term);

					$size = 'large'; // (thumbnail, medium, large, full or custom size)
					if( $image ) {
						echo wp_get_attachment_image( $image, $size );
					}
				?>
			</div><!-- .post-thumbnail -->
			<?php endif;?>
		<div class="wrapper">
			<?php
			if ( has_custom_logo() ) :
				the_custom_logo();
			endif;
			?>
			<div id="header_absolute">
				<?php if(is_singular()):?>
					<?php the_title( '<h1>', '</h1>' ); ?>
					<div id="header_excerpt">
						<?php the_excerpt(); ?>
					</div>
				<?php elseif(is_archive()):?>
					<h1><?php echo single_cat_title( '', false );?></h1>
					<?php the_archive_description( '<div id="header_excerpt">', '</div>' );?>				
				<?php endif;?>
			</div>

			<a href="#content" title="aller au contenu">
				<svg version="1.1" id="arrowDown" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">
				<g>
					<path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/>
				</g>
				</svg>
			</a>
		</div>
	</header>

	<div id="content">
	<?php if(is_singular()):?>
		<?php the_title( '<h1 class="print">', '</h1>' ); ?>
	<?php elseif(is_archive()):?>
		<h1 class="print"><?php echo single_cat_title( '', false );?></h1>
	<?php endif;?>
