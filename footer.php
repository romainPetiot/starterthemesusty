<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Susty
 */
?>
	<?php get_template_part( 'template-parts/menu');?>
	</div>

	<footer id="footer">
		<div class="wrapper">
			<div class="col">
				<ul id="right-footer" role="complementary">
					<?php if ( is_active_sidebar( 'right-footer' ) ) : ?>
						<?php dynamic_sidebar( 'right-footer' ); ?>
					<?php endif; ?>
				</ul>
				<?php
				if ( has_custom_logo() ) :
					the_custom_logo();
				endif;
				?>
				<div id="left-footer" role="complementary">
					<ul>
						<?php if ( is_active_sidebar( 'left-footer' ) ) : ?>
							<?php dynamic_sidebar( 'left-footer' ); ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>
			<ul id="bottom-footer" role="complementary">
				<?php if ( is_active_sidebar( 'bottom-footer' ) ) : ?>
					<?php dynamic_sidebar( 'bottom-footer' ); ?>
				<?php endif; ?>
			</ul>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>

